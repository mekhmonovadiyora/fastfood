# FastFood

Fast food restaurant landing page

### Used technologies

- [ ] [React](https://react.dev/)
- [ ] [TypeScript](https://www.typescriptlang.org/)
- [ ] [Vite](https://vitejs.dev/)
- [ ] [Sass](https://sass-lang.com/)

---

Node v20.9.0
