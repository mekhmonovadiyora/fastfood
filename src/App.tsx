import "./App.css";

import Carousel from "./components/Carousel/Carousel";
import Header from "./components/Header/Header";
import Main from "./layouts/Main";
import Tabs from "./components/Tabs/Tabs";

function App() {
  return (
    <Main>
      <Header />
      <Carousel />
      <Tabs />
    </Main>
  );
}

export default App;
