import "./main.scss";

import { ReactNode } from "react";

type mainProps = {
  children: ReactNode;
};

export default function Main({ children }: mainProps) {
  return <main className="main">{children}</main>;
}
