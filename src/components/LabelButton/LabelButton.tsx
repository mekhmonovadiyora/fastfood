import "./label-button.scss";

type buttonProps = {
  label: string;
};

export default function LabelButton({ label }: buttonProps) {
  return <button className="button">{label}</button>;
}
