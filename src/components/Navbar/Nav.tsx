import "./navbar.scss";
const navElements = ["Меню", "Филиалы", "О нас", "Контакты"];

export default function Nav() {
  return (
    <nav className="navbar">
      {navElements?.map((navEl) => (
        <a key={navEl} className="link">
          {navEl}
        </a>
      ))}
    </nav>
  );
}
