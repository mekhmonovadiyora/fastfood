import "../../sassStyles/_global.scss";

import Content from "../Content/Content";
import LabelButton from "../LabelButton/LabelButton";
import foods from "../../utils/foods";
import { useState } from "react";

export default function Tabs() {
  const [activeTab, setActiveTab] = useState(0);
  return (
    <div className="tabs-container">
      <div className="container">
        {foods?.map((food, i) => {
          if (i == activeTab) {
            return <LabelButton label={food?.name} />;
          }
          return (
            <p
              onClick={() => {
                setActiveTab(i);
              }}
            >
              {food?.name}
            </p>
          );
        })}
      </div>

      <Content data={foods[activeTab]} />
    </div>
  );
}
