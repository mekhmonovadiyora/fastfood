import "./header.scss";

import LabelButton from "../LabelButton/LabelButton";
import Logo from "../Logo/Logo";
import Nav from "../Navbar/Nav";

export default function Header() {
  return (
    <header className="header">
      <div className="left-container">
        <Logo />
        <Nav />
      </div>
      <div className="right-container">
        <LabelButton label="Войти" />
      </div>
    </header>
  );
}
