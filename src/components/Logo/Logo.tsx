import "./logo.scss";

import logo from "../../../public/vite.svg";

export default function Logo() {
  return <img className="logo" src={logo} />;
}
