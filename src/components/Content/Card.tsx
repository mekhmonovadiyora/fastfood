import "./content.scss";

type cardTypes = {
  name: string;
  price: string;
  details: string;
  image?: string;
};

export default function Card({ name, price, details, image }: cardTypes) {
  return (
    <div className="card-container">
      <img src={image} />
      <h4 className="card-title">{name}</h4>
      <p className="details">{details}</p>
      <p>
        <span className="price-title">
          {price} <span className="details">сум</span>
        </span>
      </p>
    </div>
  );
}
