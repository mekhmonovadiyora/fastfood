import "../../sassStyles/_global.scss";

import Card from "./Card";

type contentTypes = {
  data: {
    name: string;
    items: {
      name: string;
      details: string;
      price: string;
      image: string;
    }[];
  };
};

export default function Content({ data }: contentTypes) {
  return (
    <div className="content-container">
      <h2 className="title">{data?.name}</h2>
      <div className="container">
        {data?.items?.map(
          (item: {
            name: string;
            price: string;
            details: string;
            image: string;
          }) => (
            <Card
              key={item?.name}
              name={item?.name}
              price={item.price}
              details={item.details}
              image={item.image}
            />
          )
        )}
      </div>
    </div>
  );
}
