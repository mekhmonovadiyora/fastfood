import "./carousel.scss";

import food from "../../assets/png/food.png";

export default function Carousel() {
  return (
    <div className="slider-container">
      <div className="slider-wrapper">
        <div className="slider-slide">
          <img src={food} alt="Slide 1" />
        </div>
        <div className="slider-slide">
          <img src={food} alt="Slide 2" />
        </div>
        <div className="slider-slide">
          <img src={food} alt="Slide 3" />
        </div>
      </div>
    </div>
  );
}
