import burger from "../assets/png/burger.png";
import klab from "../assets/png/klab.png";
import lavash1 from "../assets/png/lavash1.png";
import lavash2 from "../assets/png/lavash2.png";
import lavash3 from "../assets/png/lavash3.png";
import shaurma from "../assets/png/shaurma.png";

export default [
  {
    name: "Пицца",
    items: [
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        price: "39 000",
        image: burger,
      },
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        price: "39 000",
        image: burger,
      },
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        price: "39 000",
        image: burger,
      },
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        price: "39 000",
        image: burger,
      },
    ],
  },
  {
    name: "Паста",
    items: [
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        price: "39 000",
        image: lavash1,
      },
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        price: "39 000",
        image: lavash1,
      },
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        price: "39 000",
        image: lavash1,
      },
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        price: "39 000",
        image: lavash1,
      },
    ],
  },
  {
    name: "Закуски",
    items: [
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        price: "39 000",
        image: lavash2,
      },
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        price: "39 000",
        image: lavash2,
      },
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        price: "39 000",
        image: lavash2,
      },
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        price: "39 000",
        image: lavash2,
      },
    ],
  },
  {
    name: "Синнамон-роллы",
    items: [
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        price: "39 000",
        image: lavash3,
      },
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        price: "39 000",
        image: lavash3,
      },
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        price: "39 000",
        image: lavash3,
      },
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        price: "39 000",
        image: lavash3,
      },
    ],
  },
  {
    name: "Напитки",
    items: [
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        image: klab,
        price: "39 000",
      },
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        image: klab,
        price: "39 000",
      },
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        image: klab,
        price: "39 000",
      },
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        image: klab,
        price: "39 000",
      },
    ],
  },
  {
    name: "Гарниры",
    items: [
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        price: "39 000",
        image: shaurma,
      },
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        price: "39 000",
        image: shaurma,
      },
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        price: "39 000",
        image: shaurma,
      },
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        price: "39 000",
        image: shaurma,
      },
    ],
  },
  {
    name: "Салаты",
    items: [
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        price: "39 000",
        image: burger,
      },
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        price: "39 000",
        image: burger,
      },
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        price: "39 000",
        image: burger,
      },
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        price: "39 000",
        image: burger,
      },
    ],
  },
  {
    name: "Соус",
    items: [
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        price: "39 000",
        image: klab,
      },
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        price: "39 000",
        image: klab,
      },
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        price: "39 000",
        image: klab,
      },
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        price: "39 000",
        image: klab,
      },
    ],
  },
  {
    name: "Еще",
    items: [
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        price: "39 000",
        image: shaurma,
      },
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        price: "39 000",
        image: shaurma,
      },
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        price: "39 000",
        image: shaurma,
      },
      {
        name: "Комбо 1",
        details: "Мясной лаваш, фри, кола разлив и кетчуп",
        price: "39 000",
        image: shaurma,
      },
    ],
  },
];
